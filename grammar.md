## Sahasra Grammar
-------------------

**keywords**

KEYWORDS = {var, con, sqrt}

**numbers**

All numbers 0-9 including numbers using decimal places (_NOTE: no imaginary numbers for now_)

**identifiers**

All strings composed of the set A-Z, a-z and 0-9. Except any strings which begin with numbers.

**binary_operators**

BINARY_OPERATOR = {+, -, *, /}

**unary_operator**

UNARY_OPERATOR = {"sqrt"}

**separator**

SEPARATOR = {,}

**Language Rules**

var <identifier> = <number|identifier|expression> <binary_operator> <number|identifier|expression>;

var <identifier> = <unary_operator> <number|identifier|expression>;

con <identifer> = <unary_operator> <number|identifier|expression>;

con <identifer> = <number|identifier|expression> <binary_operator> <number|identifier|expression>;

<identifier> =  <number|identifier|expression>;

<identifer> = <unary_operator> <number|identifier|expression>;

<identifer> = <number|identifier|expression> <binary_operator> <number|identifier|expression>;

// Need to change a lot of code on how it compiles and tokenizes
function run(program) {
	var memory = new ArrayBuffer(1024);
	var memv = new DataView(memory); // holds all memory
	var vartable = new Map(); // holds references to literals and memory locations
	var memloc; // needs to be undefined since it will be reused for all allocations
	var memlabel; // holds the label name which will eventually go into the vartable key
	var memval; // holds the value which needs to be placed in memory location

	var current_statement = 0;
	while(current_statement < program.length) {
		var current_token = 0;
		var statement = program[current_statement].parsedTokens;
		while(current_token < statement.length) {
			switch(statement[current_token].symbol) {
			case "VA":
				memloc = Math.floor(Math.random() * 1023);
				current_token = current_token + 1;
				memlabel = statement[current_token].value;
				current_token = current_token + 1;
				current_token = current_token + 1; // skip to the value part
				memval = Number.parseInt(statement[current_token].value, 10); // assuming that all numbers are within 8 bit range and it is not a literal
				memv.setInt8(memloc, memval);
				vartable.set(memlabel, memloc);
				memloc = undefined;
				memlabel = undefined;
				memval = undefined;
				break;
			case "CO":
				memloc = Math.floor(Math.random() * 1023);
				current_token = current_token + 1;
				memlabel = statement[current_token].value;
				current_token = current_token + 1;
				current_token = current_token + 1; // skip to the value part
				memval = Number.parseInt(statement[current_token].value, 10); // assuming that all numbers are within 8 bit range and it is not a literal
				memv.setInt8(memloc, memval);
				vartable.set(memlabel, memloc);
				memloc = undefined;
				memlabel = undefined;
				memval = undefined;
				break;
			case "LITERAL":
				break;
			case "OP":
				break;
			default:
				// do nothing
				break;
			};
			current_token = current_token + 1;
		}
		current_statement = current_statement + 1;
	}
	var executionResult = {};
	var iterator = vartable.entries();
	var moreItems = iterator.next();
	while(moreItems.done === false) {
		executionResult[moreItems.value[0]] = memv.getInt8(moreItems.value[1]);
		moreItems = iterator.next();
	}
	return JSON.stringify(executionResult);
}

function linearize_program(program) {
	var rxNewLine = new RegExp("\n","gi");
	return program.replace(rxNewLine, "");
}

function statementize_program(program) {
	return program.split(";");
}

function tokenize_statement(statement) {
	return statement.split(" ");
}

function compile(program) {
	var linear = linearize_program(program);
	var statements = statementize_program(linear);
	var programTree = statements.map(process_statement);
	return programTree;
}

function process_statement(statement, lineNumber) {
	var statementObject = {};
	statementObject.lineNumber = lineNumber;
	var tokens = tokenize_statement(statement);
	var parsedTokens = tokens.map(parse);
	statementObject.parsedTokens = parsedTokens;
	return statementObject;
}

function cleanTokens(tokens) {

}

function parse(token) {
	var desc;
	switch(token) {
	case "va":
		desc = {symbol: "VA"};
		break;
	case "co":
		desc = {symbol: "CO"};
		break;
	case "=":
		desc = {symbol: "OP", value: token, operation: "assign"};
		break;
	default:
		desc = {symbol: "LITERAL", value: token};
		break;
	};
	return desc;
}

function check_syntax(progtree) {
	var errorflag = false;
	var errormsg = "";
	var current_statement = 0;
	while(errorflag === false && current_statement < progtree.length) {
		var statement = progtree[current_statement].parsedTokens;
		var current_token = 0;
		while(errorflag === false && current_token < statement.length - 1) {
			switch(statement[current_token].symbol) {
			case "VA":
				errorflag = ((statement[current_token + 1].symbol) !== "LITERAL");
				errormsg = errorflag === true ? "a variable should be followed by a literal" : undefined;
				break;
			case "CO":
				errorflag = ((statement[current_token + 1].symbol) !== "LITERAL");
				errormsg = errorflag === true ? "a constant should be followed by a literal" : undefined;
				break;
			case "OP":
				errorflag = ((statement[current_token + 1].symbol) !== "LITERAL");
				errormsg = errorflag === true ? "an operation should be followed by a literal" : undefined;
				break;
			case "LITERAL":
				errorflag = ((statement[current_token + 1].symbol) !== "OP");
				errormsg = errorflag === true ? "a literal should be followed by a operation" : undefined;
				break;
			default:
				errorflag = false;
				errormsg = errorflag === true ? "unknown symbol" : undefined;
				break;
			}
			current_token = current_token + 1;
		}
		current_statement = current_statement + 1;
	}
	if (errorflag === false) {
		return "passed!";
	} else {
		return errormsg;
	}
}

var prog = `
va in = 10;`;

module.exports.compile = compile;
module.exports.checkSyntax = check_syntax;
module.exports.run = run;

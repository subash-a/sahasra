# README #

This is an implementation of a toy compiler which can compile and execute programs which intend to do number crunching (only). This is for learning purposes only and any use of this language for production is strictly at your own risk.

### Current Progress ###

Rewriting the compiler for better maintainability.

### Contribution guidelines ###

If you want to contribute mail me at s7subash at google mail dot com
